﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Informações gerais sobre um assembly são controladas através do seguinte 
' conjunto de atributos. Altere o valor destes atributos para modificar a informação
' associada a um assembly.

' Revise os valores dos atributos do assembly

<Assembly: AssemblyTitle("CBA Caixa")> 
<Assembly: AssemblyDescription("Cliente de Chamadas CBA Painel")> 
<Assembly: AssemblyCompany("Insign Digital")> 
<Assembly: AssemblyProduct("CBA Caixa")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'O GUID a seguir é para o ID da typelib se este projeto for exposto para COM
<Assembly: Guid("5cfa3aab-01c2-46e4-a7e2-4dfd2fd42288")> 

' Informação de versão para um assembly consiste nos quatro valores a seguir:
'
'      Versão Principal
'      Versão Secundária 
'      Número da Versão
'      Revisão
'
' É possível especificar todos os valores ou usar o padrão de números da Versão e Revisão 
' utilizando o '*' como mostrado abaixo:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
