﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fConfig
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: O procedimento a seguir é exigido pelo Windows Form Designer
    'Ele pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.lCaixaInput = New System.Windows.Forms.Label()
        Me.tbCaixa = New System.Windows.Forms.TextBox()
        Me.tbPorta = New System.Windows.Forms.TextBox()
        Me.lPortaInput = New System.Windows.Forms.Label()
        Me.tbServidor = New System.Windows.Forms.TextBox()
        Me.lServidorInput = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(54, 97)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancelar"
        '
        'lCaixaInput
        '
        Me.lCaixaInput.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lCaixaInput.AutoSize = True
        Me.lCaixaInput.Location = New System.Drawing.Point(28, 68)
        Me.lCaixaInput.Name = "lCaixaInput"
        Me.lCaixaInput.Size = New System.Drawing.Size(33, 13)
        Me.lCaixaInput.TabIndex = 11
        Me.lCaixaInput.Text = "Caixa"
        '
        'tbCaixa
        '
        Me.tbCaixa.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbCaixa.Location = New System.Drawing.Point(67, 65)
        Me.tbCaixa.Name = "tbCaixa"
        Me.tbCaixa.Size = New System.Drawing.Size(48, 20)
        Me.tbCaixa.TabIndex = 10
        Me.tbCaixa.Text = "1"
        '
        'tbPorta
        '
        Me.tbPorta.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbPorta.Location = New System.Drawing.Point(67, 38)
        Me.tbPorta.Name = "tbPorta"
        Me.tbPorta.Size = New System.Drawing.Size(48, 20)
        Me.tbPorta.TabIndex = 9
        Me.tbPorta.Text = "8888"
        '
        'lPortaInput
        '
        Me.lPortaInput.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lPortaInput.AutoSize = True
        Me.lPortaInput.Location = New System.Drawing.Point(29, 41)
        Me.lPortaInput.Name = "lPortaInput"
        Me.lPortaInput.Size = New System.Drawing.Size(32, 13)
        Me.lPortaInput.TabIndex = 8
        Me.lPortaInput.Text = "Porta"
        '
        'tbServidor
        '
        Me.tbServidor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbServidor.Location = New System.Drawing.Point(67, 12)
        Me.tbServidor.Name = "tbServidor"
        Me.tbServidor.Size = New System.Drawing.Size(117, 20)
        Me.tbServidor.TabIndex = 7
        Me.tbServidor.Text = "127.0.0.1"
        '
        'lServidorInput
        '
        Me.lServidorInput.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lServidorInput.AutoSize = True
        Me.lServidorInput.Location = New System.Drawing.Point(15, 15)
        Me.lServidorInput.Name = "lServidorInput"
        Me.lServidorInput.Size = New System.Drawing.Size(46, 13)
        Me.lServidorInput.TabIndex = 6
        Me.lServidorInput.Text = "Servidor"
        '
        'Config
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(212, 138)
        Me.Controls.Add(Me.lCaixaInput)
        Me.Controls.Add(Me.tbCaixa)
        Me.Controls.Add(Me.tbPorta)
        Me.Controls.Add(Me.lPortaInput)
        Me.Controls.Add(Me.tbServidor)
        Me.Controls.Add(Me.lServidorInput)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Config"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CBA Caixa - Assistente de Configuração"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents lCaixaInput As System.Windows.Forms.Label
    Friend WithEvents tbCaixa As System.Windows.Forms.TextBox
    Friend WithEvents tbPorta As System.Windows.Forms.TextBox
    Friend WithEvents lPortaInput As System.Windows.Forms.Label
    Friend WithEvents tbServidor As System.Windows.Forms.TextBox
    Friend WithEvents lServidorInput As System.Windows.Forms.Label

End Class
