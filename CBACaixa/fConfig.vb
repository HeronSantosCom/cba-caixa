﻿Imports System.Windows.Forms
Imports Microsoft.Win32
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Public Class fConfig

    Private Sub fConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CarregaConfig()
    End Sub

    Private Sub CarregaConfig(Optional ByVal Tentativa As Boolean = False)
        Me.Hide()
        Try
            Dim Servidor As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\CBACaixa", "Servidor", Nothing).ToString()
            Dim Porta As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\CBACaixa", "Porta", Nothing).ToString()
            Dim nCaixa As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\CBACaixa", "Caixa", Nothing).ToString()
            IniciaSocket(Servidor, Porta, nCaixa, Tentativa)
        Catch ex As Exception
            'MsgBox(ex.Message)
            ShowConfig()
        End Try
    End Sub

    Private Sub ShowConfig()
        Me.TopMost = True
        Me.Show()
        Me.BringToFront()
        Me.Focus()
        Me.Activate()
    End Sub

    Private Sub SalvaConfig(ByVal Servidor As String, ByVal Porta As String, ByVal Caixa As String)
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Try
            My.Computer.Registry.CurrentUser.CreateSubKey("Software\CBACaixa")
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\CBACaixa", "Servidor", Servidor, RegistryValueKind.String)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\CBACaixa", "Porta", Porta, RegistryValueKind.String)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\CBACaixa", "Caixa", Caixa, RegistryValueKind.String)
            Dim BoxConfig As MsgBoxResult = MsgBox("Testar configuração?", MsgBoxStyle.YesNo)
            If BoxConfig = MsgBoxResult.Yes Then
                CarregaConfig(True)
            Else
                Sair()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub IniciaSocket(ByVal Servidor As String, ByVal Porta As String, ByVal Caixa As String, Optional ByVal Tentativa As Boolean = False)
        Dim CBASocket As New TcpClient
        Dim CBAPainel As NetworkStream
        Try
            CBASocket.Connect(IPAddress.Parse(Servidor), Porta)
            CBAPainel = CBASocket.GetStream
            Dim nCaixa As Byte() = Encoding.ASCII.GetBytes(Caixa & "$")
            CBAPainel.Write(nCaixa, 0, nCaixa.Length)
            CBAPainel.Flush()
            CBASocket.Close()
            Sair()
        Catch ex As Exception
            Dim BoxRetry As MsgBoxResult = MsgBox(ex.Message & vbCrLf & vbCrLf & "Deseja tentar novamente?", MsgBoxStyle.RetryCancel)
            If BoxRetry = MsgBoxResult.Retry Then
                CarregaConfig()
            Else
                Dim BoxConfig As MsgBoxResult = MsgBox("Deseja editar as configurações?", MsgBoxStyle.YesNo)
                If BoxConfig = MsgBoxResult.No Then
                    Sair()
                Else
                    If Tentativa = True Then
                        Me.tbServidor.Text = Servidor
                        Me.tbPorta.Text = Porta
                        Me.tbCaixa.Text = Caixa
                        ShowConfig()
                    End If
                End If
            End If
        End Try
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Try
            Dim Servidor As String = Me.tbServidor.Text
            Dim Porta As String = Me.tbPorta.Text
            Dim Caixa As String = Me.tbCaixa.Text
            SalvaConfig(Servidor, Porta, Caixa)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Sair()
    End Sub


    Private Sub Sair()
        If Me.InvokeRequired Then
            Me.Invoke(New Action(AddressOf Sair))
            Return
        End If
        If (System.Windows.Forms.Application.MessageLoop) Then
            System.Windows.Forms.Application.Exit()
        Else
            System.Environment.Exit(1)
        End If
        End
    End Sub
End Class
